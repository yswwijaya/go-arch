package config

import (
	"github.com/spf13/viper"
	"log"
)

type ConnConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
}

func UserConfig() ConnConfig {

	viper.SetConfigFile("config/config.json")

	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		log.Println("Service RUN on DEBUG mode")
	}

	dbHost := viper.GetString(`database.host`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.name`)

	config := ConnConfig{
		Host: dbHost,
		Port: dbPort,
		User: dbUser,
		Password: dbPass,
		DBName: dbName,
	}

	return config

}