package mysql

import (
	"database/sql"
	"fmt"
	"net/url"
	"time"

	_ "github.com/go-sql-driver/mysql"
	cn "go-arch/config"
)

var db *sql.DB
var err error

func SqlInit() (*sql.DB, error) {

	conf := cn.UserConfig()

	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", conf.User, conf.Password, conf.Host, conf.Port, conf.DBName)

	val := url.Values{}

	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())

	dbConn, err := sql.Open(`mysql`, dsn)

	if err != nil {
		return nil, err
	}

	dbConn.SetMaxOpenConns(100)

	dbConn.SetMaxIdleConns(20)
	dbConn.SetConnMaxLifetime(30 * time.Minute)

	err = dbConn.Ping()
	if err != nil {
		return nil, err
	}

	return dbConn, nil
}