package sqlx

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	cn "go-arch/config"
)

type (
	ISqlx interface {
		Exec(query string, args ...interface{}) (sql.Result, error)
		NamedExec(query string, arg interface{}) (sql.Result, error)
		Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
		QueryRowx(query string, args ...interface{}) *sqlx.Row
		NamedQuery(query string, arg interface{}) (*sqlx.Rows, error)
		Get(dest interface{}, query string, args ...interface{}) error
		Select(dest interface{}, query string, args ...interface{}) error
	}
)


func SQLXInit() (*sqlx.DB,error) {

	conf := cn.UserConfig()

	sqlconn := fmt.Sprintf("%s:%s@(%s:%s)/pgw?parseTime=true", conf.User, conf.Password, conf.Host, conf.Port )

	DB, err := sqlx.Connect("mysql", sqlconn)

	if err != nil {
		return nil, err
	}

	return DB, err
}
