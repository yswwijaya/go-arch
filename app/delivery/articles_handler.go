package delivery

import (
	"context"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	_articleRepo "go-arch/app/repositories"
	_articleUcase "go-arch/app/usecases"
	"go-arch/config"
	"net/http"
	"strconv"
	"time"
)

// ResponseError represent the reseponse error struct
type ResponseError struct {
	Message string `json:"message"`
}

type ArticleHandler struct {
	AUsecase _articleRepo.Repository
}

// NewArticleHandler will initialize the articles/ resources endpoint
func NewArticleHandler(e *echo.Echo) {

	ar := _articleRepo.NewMysqlArticleRepository()

	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second

	au := _articleUcase.NewArticleUsecase(ar, timeoutContext)

	handler := &ArticleHandler{
		AUsecase: au,
	}

	e.GET("/articles", handler.FetchArticle)

}

// FetchArticle will fetch the article based on given params
func (a *ArticleHandler) FetchArticle(c echo.Context) error {
	numS := c.QueryParam("num")
	num, _ := strconv.Atoi(numS)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	listAr,  err := a.AUsecase.Fetch(ctx, int64(num))

	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	c.Response().Header().Set(`X-Cursor`, "nextCursor")

	return c.JSON(http.StatusOK, listAr)
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}
	logrus.Error(err)
	switch err {
	case config.ErrInternalServerError:
		return http.StatusInternalServerError
	case config.ErrNotFound:
		return http.StatusNotFound
	case config.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}