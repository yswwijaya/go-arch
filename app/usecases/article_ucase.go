package usecases

import (
	"context"
	"go-arch/app/entities"
	article "go-arch/app/repositories"
	"time"
)

type articleUsecase struct {
	articleRepo    article.Repository
	contextTimeout time.Duration
}

// NewArticleUsecase will create new an articleUsecase object representation of article.Usecase interface
func NewArticleUsecase(a article.Repository,  timeout time.Duration) article.Repository {
	return &articleUsecase{
		articleRepo:    a,
		contextTimeout: timeout,
	}
}

func (a *articleUsecase) Fetch(c context.Context, num int64) ([]*entities.Article, error) {
	if num == 0 {
		num = 10
	}

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	listArticle,  err := a.articleRepo.Fetch(ctx, num)
	if err != nil {
		return nil, err
	}

	return listArticle,  nil
}